import os
import pyudev
from time import sleep

def loadData(device):
	print "load Data"
	os.system("killall -9 chromium")
	os.system("mount "+device+" /media/usb")
	os.system("cp /music/usb/*.json /home/pi/standbyyou/exports/")
	os.system("umount /music/usb")
	os.system("chromium --allow-file-access-from-files --noerrdialogs --kiosk file:///home/pi/standbyyou/index.html")

def checkForUSBDevice(name):
	res = ""
	context = pyudev.Context()
	for device in context.list_devices(subsystem='block', DEVTYPE='partition'):
		if device.get('ID_FS_LABEL') == name:
			res = device.device_node
			return res
def main():
	## MPD object instance
	while True:
		device = checkForUSBDevice("iamaKey") # 1GB is the name of my thumb drive
		if device != "":
			# USB thumb drive has been inserted, new music will be copied
			loadData(device)
			while checkForUSBDevice("iamaKey") == device:
				sleep(1.0)

	sleep(0.1)

# Script starts here
if __name__ == "__main__":
	main()